use std::io::{Error, ErrorKind, Result};

pub struct PacketBuffer {
    pub buf: [u8; 512], // packet
    pub pos: usize,     // Processed position within the buffer
}

macro_rules! io_err {
    ($msg:expr) => {
        Err(Error::new(ErrorKind::Other, $msg))
    };
}

impl PacketBuffer {
    pub fn new() -> PacketBuffer {
        PacketBuffer {
            buf: [0; 512],
            pos: 0,
        }
    }

    /// Add byte and increment position
    ///
    /// # Arguments
    ///
    /// * `val` - byte to be written to the buffer
    pub fn write_u8(&mut self, val: u8) -> Result<()> {
        if self.pos > 512 {
            return io_err!("End of buffer");
        }
        self.buf[self.pos] = val;
        self.pos += 1;
        Ok(())
    }

    /// Write word to buffer and increment position
    ///
    /// # Arguments
    ///
    /// * `val` - word to be written to the buffer
    pub fn write_u16(&mut self, val: u16) -> Result<()> {
        self.write_u8((val >> 8) as u8)?;
        self.write_u8((val & 0xFF) as u8)?;
        Ok(())
    }

    /// Write double word to buffer and increment position
    ///
    /// # Arguments
    ///
    /// * `val` - double word to be written to the buffer
    pub fn write_u32(&mut self, val: u32) -> Result<()> {
        self.write_u8(((val >> 24) & 0xFF) as u8)?;
        self.write_u8(((val >> 16) & 0xFF) as u8)?;
        self.write_u8(((val >> 8) & 0xFF) as u8)?;
        self.write_u8(((val >> 0) & 0xFF) as u8)?;
        Ok(())
    }

    /// Write label and advance buffer
    ///
    /// # Arguments
    ///
    /// * `qname` - lable to be written to the buffer
    pub fn write_qname(&mut self, qname: &str) -> Result<()> {
        for label in qname.split('.') {
            let len = label.len();
            if len > 0x3f {
                return io_err!("Single label exceeds 63 characters of length");
            }
            self.write_u8(len as u8)?;
            for b in label.as_bytes() {
                self.write_u8(*b)?;
            }
        }
        self.write_u8(0)?;
        Ok(())
    }

    /// Current position of the buffer
    pub fn pos(&self) -> usize {
        self.pos
    }

    // Increment the buffer position by steps bytes
    //
    // # Arguments
    //
    // * `steps` - number of bytes to advance the buffer position:w
    //
    pub fn step(&mut self, steps: usize) -> Result<()> {
        self.pos += steps;
        Ok(())
    }

    // Move the buffer position
    //
    // # Arguments
    //
    // * `pos` - postion in the buffer to set buffer to
    fn seek(&mut self, pos: usize) -> Result<()> {
        self.pos = pos;
        Ok(())
    }

    /// Return the buffer's current position as a byte and advance the buffer position
    fn read(&mut self) -> Result<u8> {
        if self.pos >= 512 {
            return io_err!("End of buffer");
        }
        let res = self.buf[self.pos];
        self.pos += 1;
        Ok(res)
    }

    /// Return the byte from the specified position in the buffer
    ///
    /// Does not change the buffer position
    ///
    /// # Arguments
    ///
    /// * `pos` - position to read the byte from
    fn get(&mut self, pos: usize) -> Result<u8> {
        if self.pos >= 512 {
            return io_err!("End of buffer");
        }
        Ok(self.buf[pos])
    }

    /// Return the n bytes starting from teh specified position
    ///
    /// Does not change the buffer position
    ///
    /// # Arguments
    ///
    /// * `start' - positions in the buffer to start reading from
    /// * `len` - number of bytes to be read from the buffer
    fn get_range(&mut self, start: usize, len: usize) -> Result<&[u8]> {
        if start + len >= 512 {
            return io_err!("Range will suppase the buffer length");
        }
        Ok(&self.buf[start..(start + len)])
    }

    /// Return the buffers current postion as a u16 and advance the buffer position
    pub fn read_u16(&mut self) -> Result<u16> {
        let res = (self.read()? as u16) << 8 | (self.read()? as u16);
        Ok(res)
    }

    /// Return the buffers current position as a u32 and advance the buffer position
    pub fn read_u32(&mut self) -> Result<u32> {
        let res = (self.read()? as u32) << 24
            | (self.read()? as u32) << 16
            | (self.read()? as u32) << 8
            | (self.read()? as u32);
        Ok(res)
    }

    /// Return the label at the current position and then advance the buffer
    ///
    /// # Arguments
    ///
    /// * `outstr` - string that the label will be written to
    pub fn read_qname(&mut self, outstr: &mut String) -> Result<()> {
        let mut pos = self.pos();

        let mut jumped = false;
        let max_jumps = 5;
        let mut jumps_performed = 0;

        let mut delim = "";
        loop {
            if jumps_performed > max_jumps {
                return io_err!(format!("Limit of {} jumps exceeded", max_jumps));
            }

            // labels start with a length byte
            let len = self.get(pos)?;

            // if len has the two most significant bits are set, it
            // represents a jump to some other offset within the
            // packet
            if (len & 0xC0) == 0xC0 {
                if !jumped {
                    self.seek(pos + 2)?;
                }

                let b2 = self.get(pos + 1)? as u16;
                let offset = ((len as u16 ^ 0xC0) << 8) | b2;
                pos = offset as usize;

                jumped = true;
                jumps_performed += 1;

                continue;
            } else {
                pos += 1;

                // Domain names are terminated by an empty label of length 0
                if len == 0 {
                    break;
                }

                outstr.push_str(delim);

                // Get ASCII bytes for the label and append to output buffer
                let str = self.get_range(pos, len as usize)?;
                outstr.push_str(&String::from_utf8_lossy(str).to_lowercase());

                delim = ".";
                pos += len as usize;
            }
        }

        if !jumped {
            self.seek(pos)?;
        }
        Ok(())
    }

    /// Set the byte value in the buffer at the specified position
    ///
    /// # Arguments
    ///
    /// * `pos` - position in the buffer to set the byte value
    /// * `val` - byte value to be set in the buffer
    pub fn set(&mut self, pos: usize, val: u8) -> Result<()> {
        if pos >= 512 {
            return io_err!("Exceeds buffer length");
        }
        self.buf[pos] = val;
        Ok(())
    }

    /// Set the u16 value in the buffer at the specified position
    ///
    /// # Arguments
    ///
    /// * `pos` - position in the buffer to set the u16 value
    /// * `val` - u16 value to be set in the buffer
    pub fn set_u16(&mut self, pos: usize, val: u16) -> Result<()> {
        if pos + 1 >= 512 {
            return io_err!("Exceeds buffer length");
        }
        self.set(pos, ((val >> 8) & 0xFF) as u8)?;
        self.set(pos + 1, (val & 0xFF) as u8)?;
        Ok(())
    }
}
