use std::io::Result;
use std::net::UdpSocket;

use common::{
    dns::{DnsPacket, DnsQuestion, QueryType},
    io::PacketBuffer,
};

fn main() -> Result<()> {
    // build the request
    let qname = "google.com";
    let qtype = QueryType::MX;
    let mut packet = DnsPacket::new();

    packet.header.id = 6666;
    packet.header.num_questions = 1;
    packet.header.recursion_desired = true;
    packet
        .questions
        .push(DnsQuestion::new(qname.to_string(), qtype));

    // send the requests to google's public DNS
    let mut req_buffer = PacketBuffer::new();
    packet.write(&mut req_buffer)?;

    let server = ("8.8.8.8", 53);
    let socket = UdpSocket::bind(("0.0.0.0", 43210))?;
    socket.send_to(&req_buffer.buf[0..req_buffer.pos], server)?;

    // receive response
    let mut res_buffer = PacketBuffer::new();
    socket.recv_from(&mut res_buffer.buf)?;

    // deserialise response
    let res_packet = DnsPacket::from_buffer(&mut res_buffer)?;
    println!("{:#?}", res_packet.header);

    for q in res_packet.questions {
        println!("{:#?}", q);
    }

    for rec in res_packet.answers {
        println!("{:#?}", rec);
    }

    for rec in res_packet.authorities {
        println!("{:#?}", rec);
    }

    for rec in res_packet.resources {
        println!("{:#?}", rec);
    }

    Ok(())
}
