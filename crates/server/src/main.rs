use common::dns::DnsPacket;
use common::io::PacketBuffer;
use std::fs::File;
use std::io::{Read, Result};

fn main() -> Result<()> {
    let mut f = File::open("local/response_packet.txt")?;
    let mut buffer = PacketBuffer::new();
    f.read(&mut buffer.buf)?;

    let packet = DnsPacket::from_buffer(&mut buffer)?;
    println!("{:#?}", packet.header);

    for q in packet.questions {
        println!("{:#?}", q);
    }

    for an in packet.answers {
        println!("{:#?}", an);
    }

    for au in packet.authorities {
        println!("{:#?}", au);
    }

    for r in packet.resources {
        println!("{:#?}", r);
    }

    Ok(())
}
