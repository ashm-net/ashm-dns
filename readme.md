# Ashm dns

Basic dns server implemented in rust, the motivation is around expanding my knowledge of programming in rust, gainaing a better understanding of how dns works and expanding my knowledge of network programming.

## Basics

- Development language is rust
- Three main components:
  - Server: dns server implementationt
  - Client: clinet that can be used to resolve queries against a dns server
  - Common: crate that contains the common strcutures use in both the client and the server

## References

- [Server tutorial](https://github.com/EmilHernvall/dnsguide/tree/master)
